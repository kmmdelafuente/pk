$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var mobile = $("input#mobile").val();
            var message = $("textarea#message").val();
            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "././rsvp.php",
                type: "POST",
                data: {
                    name: name,
                    mobile: mobile,
                    email: email,
                    message: message,
                    rsvptype: $('[name="optionsRadios"]:checked').val(),
                    attend: $('[name="attendRadios"]:checked').val(),
                    guests: $("#pax").val(),
                    submit: $("#submitType").val() == "submit"
                },
                cache: false,
                success: function(response) {
                    var resp = jQuery.parseJSON(response);
                    if (resp.retrieve) {
                        var extData = jQuery.parseJSON(resp.data);
                        setExistingResponse(extData.NAME, extData.PHONENUMBER, extData.EMAIL, extData.ATTEND, extData.GUESTS);
                    } else if (resp.success) {
                        // Success message
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-success')
                            .append("<strong>" + resp.message + "</strong>");
                        $('#success > .alert-success')
                            .append('</div>');
                        $('html, body').animate({scrollTop: $("#contactForm").offset().top}, 2000);
                        
    
                        //clear all fields
                        $('#contactForm').trigger("reset");
                        checkAttendFields();
                    } else {
                        $('#success').html("<div class='alert alert-" + resp.error_type + "'>");
                        $('#success > .alert-' + resp.error_type).html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-' + resp.error_type)
                            .append("<strong>" + resp.message + "</strong>");
                        $('#success > .alert-' + resp.error_type)
                            .append('</div>');
                        $('html, body').animate({scrollTop: $("#contactForm").offset().top}, 2000);
                        //$('html, body').animate({scrollTop:$('#success').position().top}, 'slow');
                        
                        if (resp.error_cause == "exist") {
                            var extData = jQuery.parseJSON(resp.data);
                            setExistingResponse(extData.NAME, extData.PHONENUMBER, extData.EMAIL, extData.ATTEND, extData.GUESTS);
                        }
                    }
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    $('html, body').animate({scrollTop:$('#success').position().top}, 'slow');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
