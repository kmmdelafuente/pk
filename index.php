<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="monogram.ico">

    <title>Philip x Kristel</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Wedding CSS -->
    <link href="css/wedding.css" rel="stylesheet">
    
    
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.default.min.css" rel="stylesheet">
    <link href="css/bootstrap-modal-carousel.min.css" rel="stylesheet">
        
    <link href="css/mb-comingsoon.min.css" rel="stylesheet" />
    
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script type="text/javascript">
        function mainOwl() {
			var owl = $("#instafeed").data('owlCarousel');
            if(typeof owl != 'undefined') {
				owl.destroy();
			}
            var feed = new Instafeed({
			        get: 'tagged',
			        tagName: 'philipxkristel',
			        clientId: 'dc5e137829fe492c97668abbaa6acf0a',
			        accessToken: '1565987.467ede5.df4589b73d6d4c70803a7175f2f6192c',
			        template: '<div class="item"><img src="{{image}}" /><div class="itemcaption">{{caption}}</div></div>',
					resolution: 'low_resolution',
					sortBy: 'most-recent',
					limit: 10,
			        after: function() {
			            $("#instafeed").owlCarousel({
                            loop: true,
                            center: true,
                            margin: 10,
                            autoplay: true,
                            autoplayTimeout: 3000,
                            responsiveClass: true,
                            nav: true,
                            responsive: {
                              0: { items: 1 },
                              600: { items: 3 },
                              1000: {
                                items: 5,
                                margin: 20
                              }
                            }
                        });
                        owl = $("#instafeed").data('owlCarousel');
			        }
			    }); 
			feed.run();
			        
			intId = setInterval(function() {
					$( "#instafeed" ).fadeOut( "slow", function() {
						if(typeof owl != 'undefined') {
							owl.destroy();
						}
						$( "#instafeed" ).empty();
						if(feed.hasNext()) {
                        	feed.next(); 
                        } else {
                        	feed.run();
                        }
                        $( "#instafeed" ).fadeIn( "slow");
					});
				}, 30000);
        }
        
        function fullOwl() {
            var owl = $("#full-owl").data('owlCarousel');
            if(typeof owl != 'undefined') {
				owl.destroy();
			}
            var feed2 = new Instafeed({
    			        get: 'tagged',
    			        tagName: 'philipxkristel',
    			        target: 'full-owl',
    			        clientId: 'dc5e137829fe492c97668abbaa6acf0a',
    			        accessToken: '1565987.467ede5.df4589b73d6d4c70803a7175f2f6192c',
    			        template: '<div class="item"><img src="{{image}}" class="img-responsive img-centered" /></div>',
    					resolution: 'standard_resolution',
    					sortBy: 'most-recent',
    					limit: 5,
    			        after: function() {
    			            $("#full-owl").owlCarousel({
                                loop: true,
                                center: true,
                                margin: 10,
                                autoplay: true,
                                autoplayTimeout: 3000,
                                singleItem: true,
                                items: 1,
    					        dots: false,
                                autoHeight: true
                            });
                            owl = $("#full-owl").data('owlCarousel');
    			        }
    			    }); 
    		feed2.run();
            
            intId = setInterval(function() {
					$( "#full-owl" ).fadeOut( "slow", function() {
						if(typeof owl != 'undefined') {
							owl.destroy();
						}
						$( "#full-owl" ).empty();
						if(feed2.hasNext()) {
                        	feed2.next(); 
                        } else {
                        	feed2.run();
                        }
                        $( "#full-owl" ).fadeIn( "slow");
					});
				}, 30000);
        }
		
		function checkRsvpFields() {
			var radioVal = $('[name="optionsRadios"]:checked').val();
			if (radioVal == 'new') {
				$('#contactForm').trigger("reset");
				$('#nameDiv').show();
				$('#phoneDiv').show();
				$('#emailDiv').show();
				$('#attendDiv').show();
				$('#sendBtn').show();
				$('#retrieveBtn').hide();
				if ($('[name="attendRadios"]:checked').val() == "yes") {
					$('#paxDiv').show();
				} else {
					$('#paxDiv').hide();
				}
				$('#contactForm').trigger("reset");
				$("#submitType").val("submit");
				
				$("#attendRadios1").prop("checked", true);
				$("#attendRadios2").prop("checked", false);
			} else if (radioVal == 'retrieve') {
				$('#emailDiv').hide();
				$('#nameDiv').hide();
				$('#paxDiv').hide();
				$('#attendDiv').hide();
				$('#sendBtn').hide();
				$('#retrieveBtn').show();
				$("#submitType").val("retrieve");
			}
		}
		
		function setExistingResponse(name, phone, email, attend, pax) {
			$("#optionsRadios1").prop("checked", false);
			$("#optionsRadios2").prop("checked", true);
			$('#nameDiv').show();
			$('#phoneDiv').show();
			$('#emailDiv').show();
			$('#attendDiv').show();
			$('#sendBtn').show();
			$('#retrieveBtn').hide();
			$("#submitType").val("submit");
			$('#name').val(name);
			$('#phone').val(phone);
			$('#email').val(email);
			if (attend == "yes") {
				$("#attendRadios1").prop("checked", true);
				$("#attendRadios2").prop("checked", false);
				$('#paxDiv').show();
				$("#pax").val(pax);
			} else {
				$("#attendRadios1").prop("checked", false);
				$("#attendRadios2").prop("checked", true);
				$('#paxDiv').hide();
			}
		}
		
		function checkAttendFields() {
			var radioVal = $('[name="attendRadios"]:checked').val();
			if (radioVal == 'yes') {
				$('#paxDiv').show();
			} else if (radioVal == 'no') {
				$('#paxDiv').hide();
			}
		}
		
        $( document ).ready(function() {
			$('[name="optionsRadios"]').change(function() {
				checkRsvpFields();
			});
			
			$('[name="attendRadios"]').change(function() {
				checkAttendFields();
			});
			
			checkRsvpFields();
			
            $('#main_monogram').corner("30px");
						  
            $('#countdown_dashboard').mbComingsoon({ expiryDate: new Date(2015, 4, 30, 14, 30), speed:100 });
			    
			mainOwl();
				
			$('#feedFull').on('show.bs.modal', function () {
				$( "#instafeed" ).empty();
			    clearInterval(intId);
			    fullOwl();
            });
            
            $('#feedFull').on('hidden.bs.modal', function () {
                $( "#full-owl" ).empty();
                clearInterval(intId);
                mainOwl();
            });
		});
    </script>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">Philip X Kristel</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#location">Location</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#attire">Attire</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#rsvp">RSVP</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#philipxkristel">#philipxkristel</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" id="main_monogram" src="img/monogram.png" alt="">
                    <div class="intro-text">
                        <span class="name">May 30, 2015</span>
                        <!--span class="skills">Web Developer - Graphic Artist - User Experience Designer</span-->
                        
                        <hr class="star-light">
                        <div id="container">
                            <div class="row centered text-center" id="countdown_dashboard"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section class="portfolio" id="location">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Location</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                		<span class="location-name">St. Jude Thaddeus Parish</span><br />
                		<span class="location-address">Gen. Malvar St., Davao City</span><br />
                		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.462501079967!2d125.602336!3d7.072253000000008!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32f96d716ed0324f%3A0xe58133b36308dadc!2sSt.+Jude+Thaddeus+Parish!5e0!3m2!1sen!2ssg!4v1421913344811" class="map" frameborder="0" style="border:0"></iframe>
                    <!--p>Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional LESS stylesheets for easy customization.</p-->
                </div>
                <div class="col-lg-4">
                		<span class="location-name">Davao Convention and Trade Center</span><br />
                		<span class="location-address">F. Torres St., Davao City</span><br />
                		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.3816586769585!2d125.60962900000001!3d7.081676000000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32f96d09fdea9457%3A0xb28587eb9a866b8b!2sDavao+Convention+and+Trade+Center!5e0!3m2!1sen!2ssg!4v1421913926110" class="map" frameborder="0" style="border:0"></iframe>
                    <!--p>Whether you're a student looking to showcase your work, a professional looking to attract clients, or a graphic artist looking to share your projects, this template is the perfect starting point!</p-->
                </div>
            </div>
        </div>
    </section>

    <!-- Portfolio Grid Section -->
    <section id="attire" class="success">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Attire</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
				<div class="col-lg-4 col-lg-offset-2">
					<img src="img/suit.png" class="img-responsive" alt="">
                </div>
                <div class="col-lg-4">
					<img src="img/dress.png" class="img-responsive" alt="">
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="rsvp" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>RSVP</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row" id="formDiv">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <form name="sentMessage" id="contactForm" novalidate>
						<input type="hidden" name="submitType" id="submitType" />
                        <div id="success"></div>
						
						<div class="row control-group">
							<div class="form-group col-xs-12 controls">
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="new" checked>
										New Response
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="retrieve">
										Modify Existing
									</label>
								</div>
							</div>
						</div>
						<br />
                        <div class="row control-group" id="mobileDiv">
                            <div class="form-group col-xs-12 controls">
								<label for="mobile" class="control-label">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile" placeholder="11-digit number, e.g. 09xx1234567" required data-validation-required-message="Please enter your mobile number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group" id="nameDiv">
                            <div class="form-group col-xs-12 controls">
								<label for="name" class="control-label">Full Name</label>
                                <input type="text" class="form-control" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group" id="emailDiv">
                            <div class="form-group col-xs-12 controls">
                                <label for="email" class="control-label">Email Address</label>
                                <input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group" id="attendDiv">
                            <div class="form-group col-xs-12 controls">
								<label class="control-label">Attendance</label>
								<div class="radio">
									<label>
										<input type="radio" name="attendRadios" id="attendRadios1" value="yes" checked="checked">
										Yes, I can attend
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="attendRadios" id="attendRadios2" value="no">
										No, I cannot attend
									</label>
								</div>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group" id="paxDiv">
                            <div class="form-group col-xs-12 controls">
								<label for="pax" class="control-label">Number of Guests Attending (based on the invitation)</label>
								<select class="form-control" id="pax" required data-validation-required-message="Please enter your name.">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group col-xs-12">
                            		<button type="submit" class="btn btn-success btn-lg" id="sendBtn">Send</button>
                            		<button type="submit" class="btn btn-success btn-lg" id="retrieveBtn">Retrieve</button>
                                <!--button type="submit" class="btn btn-success btn-lg">Send</button-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- #philipxkristel Section -->
    <section class="success" id="philipxkristel">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>#philipxkristel</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                  <div id="instafeed" class="owl-carousel"></div>
                </div>
            </div>
            <div class="row" id="fullscree-btn-row">
                <br>
                <div class="form-group col-xs-12 text-center">
                    <button type="submit" class="btn btn-outline btn-lg" data-toggle="modal" data-target="#feedFull">
                        <i class="fa fa-arrows-alt"></i>&nbsp;View in Full Screen
                    </button>
                </div>
            </div>
        </div>
    </section> 

    <!-- Footer -->
    <footer class="text-center">
        <!--div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>3481 Melrose Place<br>Beverly Hills, CA 90210</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>About Freelancer</h3>
                        <p>Freelance is a free to use, open source Bootstrap theme created by <a href="http://startbootstrap.com">Start Bootstrap</a>.</p>
                    </div>
                </div>
            </div>
        </div-->
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Philip x Kristel 2015
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visble-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
    
    <div class="cust-modal modal fade modal-fullscreen force-fullscreen" id="feedFull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <div class="owl-carousel" id="full-owl">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
        
    <!-- Contact Form JavaScript -->
    <script src="js/instafeed.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

    <!-- Corner JavaScript -->
    <script src="js/jquery.corner.js"></script>
    
    <script language="Javascript" type="text/javascript" src="js/owl.carousel.min.js"></script>
    
    <script src="js/jquery.mb-comingsoon.min.js"></script>

</body>

</html>
