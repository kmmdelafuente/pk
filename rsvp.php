<?php
	include 'db.php';
	
	// ** Email confirmation settings
	define('EMAIL_SEND', true);    					// set to true to enable email confirmation to be sent on form submision
	
	define('EMAIL_FROM', "no-reply@philipxkristel.com"); 	// confirmation from email address
	define('EMAIL_BODY_ATTEND', "Thank you for your response.\r\n\r\nWe look forward to seeing you at the reception!\r\n\r\nCheers,\r\n\r\nPhilip and Kristel"); 	// confirmation email body for those that can attend
	define('EMAIL_BODY_CANTATTEND', "Thank you for your response.\r\n\r\nWe are sorry to hear you cannot attend.\r\n\r\nCheers,\r\n\r\nPhilip and Kristel"); 	// confirmation email body for those that can't attend

	// ** Event Hosts
	define('EVENT_HOSTS', "Philip and Kristel");
	
	/*if ($included) {
		return 0;
	}*/
	
	$event_hosts = EVENT_HOSTS;
	
	function get_ip() {
		if ($_SERVER['HTTP_X_FORWARD_FOR']) {
			return $_SERVER['HTTP_X_FORWARD_FOR'];
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}
	
	function default_val(&$var, $default) {
		if ($var=='') {
			$var = $default;
		}
	}
	
	function check_mail($email) {
		if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
			list($username,$domain)=split('@',$email);
			if(!checkdnsrr($domain,'MX')) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	$data = $_POST;
    //print_r($data);
	
	//retrieve will equal true if it is a retrieve, false if it is an existing modification
	$ret = ($data['rsvptype'] == "retrieve") && ($data['submit'] == "false");
	
	//value to store a successful update/new rsvp
	$success = false;
	
	//value to store the submitbutton text
	$submitbutton = 'Submit';
	
	//value to store the return message
	$message = '';
		
    $conn = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die ('Error connecting to mysql');
    mysql_select_db(DB_NAME);
    
    if ($ret) {
        $submitbutton = 'Update';
    
        //retrieve the data
        $query = sprintf("SELECT * FROM rsvp where PHONENUMBER = '%s'", mysql_real_escape_string($data['mobile']));
        $result = mysql_query($query);
        if ($result) {
            if ($row = mysql_fetch_assoc($result)) {
                echo json_encode(array("success" => false,
                                        "retrieve" => "true",
                                        "data" => json_encode($row)));
                return;
            } else {
                //no email match
                echo json_encode(array("success" => false,
                                        "error_type" => "danger",
                                        "error_cause" => "",
                                        "message" => "Could not find existing response for mobile number {$data['mobile']}."));
                return;
            }
        }
    } else {
        //submit the data

        if ($data['rsvptype'] == "retrieve") {
            $query = sprintf("update rsvp SET EMAIL = '%s', NAME = '%s', ATTEND = '%s', PHONENUMBER = '%s', GUESTS = %d where PHONENUMBER = '%s'",
                mysql_real_escape_string($data['email']),
                mysql_real_escape_string(ucwords($data['name'])),
                mysql_real_escape_string($data['attend']),
                mysql_real_escape_string($data['mobile']),
                mysql_real_escape_string($data['guests']),
                mysql_real_escape_string($data['mobile']));
        } else {
            //retrieve the data
            $query = sprintf("SELECT * FROM rsvp where PHONENUMBER = '%s'", mysql_real_escape_string($data['mobile']));
            $result = mysql_query($query);
            if ($result) {
                if ($row = mysql_fetch_assoc($result)) {
                    echo json_encode(array("success" => false,
                                            "error_type" => "warning",
                                            "error_cause" => "exist",
                                            "message" => "Your mobile number has an existing response. Please modify the response below.",
                                            "data" => json_encode($row)));
                    return;
                }
            }
            
            $query = sprintf("INSERT INTO rsvp SET EMAIL = '%s', NAME = '%s', ATTEND = '%s', PHONENUMBER = '%s', GUESTS = %d",
                mysql_real_escape_string($data['email']),
                mysql_real_escape_string(ucwords($data['name'])),
                mysql_real_escape_string($data['attend']),
                mysql_real_escape_string($data['mobile']),
                mysql_real_escape_string($data['guests']));
        }
        
        mysql_query($query);
        $success = true;
        
        //send out a confirmation email 
        $to = $data['email'];
        $subject = "RSVP confirmation";
        if ($data['attend']=='yes') {
            $body = "Dear {$data['name']},\r\n\r\n".EMAIL_BODY_ATTEND;
            $message = "Thank you {$data['name']}, we look forward to seeing you there.";
        } else {
            $body = "Dear {$data['name']},\r\n\r\n".EMAIL_BODY_CANTATTEND;
            $message = "Thank you {$data['name']}, we are sorry to hear you cannot attend.";
        }
        $headers = "From: ".EMAIL_FROM."\r\n";
        
        if (EMAIL_SEND) {
            mail($to, $subject, $body, $headers);
        }
        
        echo json_encode(array("success" => true, "message" => $message));
        return;
    }
    
    mysql_close($conn);